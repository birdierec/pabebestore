package com.pabebestore.inventory.tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Test {

	public static void main(String[] args){
		System.out.println(changeStringDateFormat("10/01/1990"));
		
	}
	
	public static String changeStringDateFormat(String strDate){
		String result = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat truesdf = new SimpleDateFormat("yyyy-MM-dd");
	    Date convertedCurrentDate;
		try {
			convertedCurrentDate = sdf.parse(strDate);
			result = truesdf.format(convertedCurrentDate );
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
}
